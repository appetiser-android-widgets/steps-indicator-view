package com.appetiser.stepsindicatorview

import android.animation.Animator
import android.content.Context
import android.util.AttributeSet
import android.widget.LinearLayout
import android.widget.ProgressBar
import java.util.*
import android.view.animation.DecelerateInterpolator
import android.animation.ObjectAnimator
import androidx.core.content.ContextCompat
import com.appetiser.stepsindicatorview.ext.toPx


open class ProgressBarStepIndicatorView(
    context: Context,
    attributeSet: AttributeSet
) : LinearLayout(context, attributeSet) {

    companion object {
        const val DEFAULT_HEIGHT = 12
    }

    private var stepCount: Int = 4
    private var stepNumberSelected: Int = 0
    private var viewBackgroundColor = ContextCompat.getColor(context, R.color.white)
    private var progressBackground = 0
    private var actualHeight = DEFAULT_HEIGHT
    private var animating = false

    val selectedPosition: Int
        get() = stepNumberSelected

    private val weakProgressBarHashMap: WeakHashMap<Int, ProgressBar> = WeakHashMap()

    init {

        val array = context.obtainStyledAttributes(attributeSet, R.styleable.ProgressBarStepIndicatorView)
        try {
            array.getInt(R.styleable.ProgressBarStepIndicatorView_pbsi_stepsCount, stepCount).let {
                stepCount = it
            }
            array.getInt(R.styleable.ProgressBarStepIndicatorView_pbsi_stepsNumberSelected, stepCount).let {
                stepNumberSelected = it
            }
            array.getResourceId(
                R.styleable.ProgressBarStepIndicatorView_pbsi_progress_drawable,
                R.drawable.default_progress_loading
            ).let {
                progressBackground = it
            }

            array.getColor(
                R.styleable.ProgressBarStepIndicatorView_pbsi_background_color,
                viewBackgroundColor
            ).let {
                viewBackgroundColor = it
            }

            array.getInt(R.styleable.ProgressBarStepIndicatorView_pbsi_height, actualHeight).let {
                actualHeight = it
            }

        } finally {
            array.recycle()
        }

        if (stepNumberSelected > stepCount) {
            throw IndexOutOfBoundsException("StepNumber must be lesser than StepSize")
        } else if (stepCount <= 1) {
            throw IndexOutOfBoundsException("Step count must be more than 1")
        }

        orientation = HORIZONTAL
        setBackgroundColor(viewBackgroundColor)
        weightSum = stepCount.toFloat()
    }

    override fun onAttachedToWindow() {
        super.onAttachedToWindow()
        setupProgressStepViews()
    }

    fun setStepCount(stepCount: Int) {
        this.stepCount = stepCount
        weightSum = stepCount.toFloat()
    }

    fun setStepNumberSelected(position: Int) {
        this.stepNumberSelected = position
    }

    override fun invalidate() {
        super.invalidate()
        setupProgressStepViews()
    }

    private fun setupProgressStepViews() {
        removeAllViewsInLayout()
        weakProgressBarHashMap.clear()

        for (i in 1..stepCount) {
            val progressBar = ProgressBar(
                context, null,
                android.R.attr.progressBarStyleHorizontal
            )
            progressBar.progressDrawable =
                ContextCompat.getDrawable(
                    context,
                    if (progressBackground == 0) R.drawable.default_progress_loading else progressBackground
                )
            progressBar.id = i
            progressBar.max = 100
            progressBar.progress = if (i <= stepNumberSelected) 100 else 0
            val progressLayoutParams =
                LinearLayout.LayoutParams(0, actualHeight.toPx(context), 1f)
            progressLayoutParams.setMargins(10, 0, 10, 0)
            progressBar.layoutParams = progressLayoutParams
            addView(progressBar)
            weakProgressBarHashMap[i] = progressBar
        }
    }

    @Synchronized
    fun animateNext(): Int {
        checkView()

        if (stepNumberSelected < stepCount) {
            stepNumberSelected++
            val progressBar =
                weakProgressBarHashMap[stepNumberSelected]
                    ?: throw KotlinNullPointerException("Progressbar must not be null!")
            setProgressAnimate(progressBar, 100)
        }

        return stepNumberSelected
    }

    @Synchronized
    fun animateBack(): Int {
        checkView()

        if (stepNumberSelected >= 1) {
            val progressBar =
                weakProgressBarHashMap[stepNumberSelected]
                    ?: throw KotlinNullPointerException("Progressbar must not be null!")
            stepNumberSelected--
            setProgressAnimate(progressBar, 0)
        }

        return stepNumberSelected
    }

    private fun setProgressAnimate(pb: ProgressBar, progressTo: Int) {
        val animation = ObjectAnimator.ofInt(pb, "progress", pb.progress, progressTo)
        animation.duration = 500
        animation.interpolator = DecelerateInterpolator()
        animation.addListener(object : Animator.AnimatorListener {
            override fun onAnimationRepeat(animation: Animator?) {
            }

            override fun onAnimationEnd(animation: Animator?) {
                animating = false
            }

            override fun onAnimationCancel(animation: Animator?) {
                animating = false
            }

            override fun onAnimationStart(animation: Animator?) {
                animating = true
            }

        })
        animation.start()
    }

    private fun checkView() {
        if (weakProgressBarHashMap.isEmpty()) {
            throw IllegalArgumentException("Progress item is empty")
        }
    }

    override fun onDetachedFromWindow() {
        super.onDetachedFromWindow()
        weakProgressBarHashMap.clear()
        removeAllViewsInLayout()
    }

    override fun onMeasure(widthMeasureSpec: Int, heightMeasureSpec: Int) {
        super.onMeasure(widthMeasureSpec, heightMeasureSpec)
    }

    override fun onLayout(changed: Boolean, l: Int, t: Int, r: Int, b: Int) {
        super.onLayout(changed, l, t, r, b)
    }
}